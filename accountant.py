import sys
call = sys.argv [1:]

change_quote= 0
stock={}
logs= []
available_action= ["saldo", "sprzedaż", "zakup", "magazyn", "stop", "przegląd", "konto"]
balance= int(input("Kwota na start:"))

while True:
    action=input("Podaj akcję:")
    if action == "stop":
        print("Program zatrzymany")
        break
    elif action== "zakup":
        id_product = input("Podaj nazwę towaru:")
        price = int(input("Podaj cenę:"))
        qt = int(input("Podaj ilość zakupionego towaru:"))
        if price * qt > balance:
         print("Brak wystarczających środków na koncie!")
         continue
        if id_product in stock:
         stock[id_product] += qt
         print(stock)
         balance -= price + qt
         print("Saldo: {}".format(balance))
         print(stock)
         logs.append([action, id_product, price, qt])
         print(logs)
        else:
         stock[id_product] = qt
         balance -= price * qt
         print("Saldo: {}".format(balance))
         print(stock)
         logs.append([action, id_product, price, qt])
         print(logs)
    elif action=="sprzedaz":
        id_product= input("Podaj nazwę produktu:")
        price=int(input("Podaj cenę produktu:"))
        qt=int(input("Podaj ilość przedanych produktów:"))
        if id_product in stock:
            stock[id_product]-=qt
            print(stock)
            balance += price*qt
            print("Saldo:{}".format(balance))
        else:
            print("Brak produktu w magazynie!")

    elif action=="saldo":
        print("Saldo:{}".format(balance))
    elif action=="magazyn":
        print("Obecnie w magazynie masz: {}".format(stock))
    else:
        print("Dostępne akcje to {}".format(",".join(available_action[:5])))